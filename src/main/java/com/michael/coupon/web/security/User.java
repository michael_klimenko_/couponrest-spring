package com.michael.coupon.web.security;

import com.michael.coupons.utils.ClientType;

/**
 * User class describes simple user object for login purpose
 *
 * @author michael
 */
public class User {

    //==============PROPERTIES===============
    private String username;
    private String password;
    private ClientType clientType;

    //===========CONSTRUCTOR================

    public User() {
    }

    public User(String username, String password, ClientType clientType) {
        this.username = username;
        this.password = password;
        this.clientType = clientType;
    }
    
    
    //===========GETTERS & SETTERS ==========

    /**
     * Get the value of username
     *
     * @return the value of username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Set the value of username
     *
     * @param username new value of username
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Get the value of password
     *
     * @return the value of password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Set the value of password
     *
     * @param password new value of password
     */
    public void setPassword(String password) {
        this.password = password;
    }
   

    /**
     * Get the value of clientType
     *
     * @return the value of clientType
     */
    public ClientType getClientType() {
        return clientType;
    }

    /**
     * Set the value of clientType
     *
     * @param clientType new value of clientType
     */
    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }

    //================OVERRIDES==================

    @Override
    public String toString() {
        return "User{" + "username=" + username + ", password=" + password + ", clientType=" + clientType + '}';
    }
    
    
     
}
