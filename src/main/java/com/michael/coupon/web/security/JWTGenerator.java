package com.michael.coupon.web.security;

import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.ApplicationUtilities;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import com.michael.coupons.utils.Exceptions;
import com.nimbusds.jose.JWSAlgorithm; 
import com.nimbusds.jose.JWSHeader; 
import com.nimbusds.jose.JWSSigner; 
import com.nimbusds.jose.crypto.RSASSASigner; 
import com.nimbusds.jwt.JWTClaimsSet; 
import com.nimbusds.jwt.SignedJWT; 
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
 
import java.io.InputStream; 
import java.security.KeyStore; 
import java.security.interfaces.RSAPrivateKey; 
import java.util.Date; 
 
/**
 * Generates JWT. 
 */ 
public class JWTGenerator { 
 

    private final String keystore = ApplicationUtilities.CONFIG.getString("api.security.keystore.file"); 
    private final String password = ApplicationUtilities.CONFIG.getString("api.security.keystore.password"); 
    private final String alias    = ApplicationUtilities.CONFIG.getString("api.security.key.alias"); 
 

    protected String generateJWT(User user, Long myId) throws Exception {

        RSAPrivateKey privateKey = getPrivateKey(keystore, password, alias);
        
        // Create RSA-signer with the private key
        JWSSigner signer = new RSASSASigner(privateKey);

        // Prepare JWT with claims set
//        JWTClaimsSet claimsSet = new JWTClaimsSet();
//        claimsSet.setSubject(user.getName());
//        claimsSet.setClaim("email", user.getEmail());
//        claimsSet.setClaim("roles", user.getRoles());
//        claimsSet.setIssuer("wso2.org/products/mss");
//        claimsSet.setExpirationTime(new Date(new Date().getTime() + 60 * 60 * 1000)); //60 min
        // Set expiration in 60 minutes
        final Date NOW =  new Date(new Date().getTime() / 1000 * 1000);
        Date exp = new Date(NOW.getTime() + 1000 * 60 * 60);
    
        JWTClaimsSet claimsSet = new JWTClaimsSet.Builder()
        .subject(user.getUsername())
        .claim("myId", myId)
        .claim("role", user.getClientType().name())
        //.claim("clientFacade",Serializer.toString(clientFacade))
        .expirationTime(exp)
        .notBeforeTime(NOW)
        .issueTime(NOW)
        .issuer("localhost")
        //.claim("http://example.com/is_root", true)
        .build();
 
        
        log("Generaing claims: " + claimsSet);
        SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.RS256), claimsSet);
        log("Generaing signedJWT : " + signedJWT.toString());
        // Compute the RSA signature
        signedJWT.sign(signer);
        
        // To serialize to compact form, produces something like
        // eyJhbGciOiJSUzI1NiJ9.SW4gUlNBIHdlIHRydXN0IQ.IRMQENi4nJyp4er2L
        // mZq3ivwoAjqa1uUkSBKFIX7ATndFF5ivnt-m8uApHO4kfIFOrW7w2Ezmlg3Qd
        // maXlS9DhN0nUk_hGI3amEjkKd0BWYCB8vfUbUv0XGjQip78AI4z1PrFRNidm7
        // -jPDm5Iq0SZnjKjCNS5Q15fokXZc8u0A
         
        return signedJWT.serialize();
       
    }
 
 
    private RSAPrivateKey getPrivateKey(String keyStorePath, String keyStorePassword, String alias) { 
 
        
        InputStream inputStream = null; 
        try { 
            File file = new File(keyStorePath);
            log("Loading private key: {} from file: {} " + file.getAbsolutePath());
            
            if (file.exists()) {
                inputStream = new BufferedInputStream(new FileInputStream(file));
            } else {
                inputStream = getClass().getResourceAsStream(keyStorePath);
            }

            log("Trying to get InputStream : "+ inputStream.toString());
            if (inputStream != null) {
            KeyStore keystoreObj = KeyStore.getInstance(KeyStore.getDefaultType()); 
            keystoreObj.load(inputStream, keyStorePassword.toCharArray()); 
            log("Trying to load private key : "+ keyStorePath +" "+ keyStorePassword +" "+ alias + " from keystore : " + keystoreObj.toString());
            RSAPrivateKey privateKey = (RSAPrivateKey) keystoreObj.getKey(alias, keyStorePassword.toCharArray()); 
            return privateKey; 
            } else  {
                    throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE, "Cannot obtain keystore file: " + keyStorePath);
            }
            
        } catch (Exception ex){
               throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE, ex);
            } finally { 
            if (inputStream != null) { 
                try { 
                    inputStream.close();
                } catch (Exception ex) {
                    throw new ApplicationException(Exceptions.INVALID_LOGIN_TYPE, ex);
                }
            } 
        } 
    }
   
}