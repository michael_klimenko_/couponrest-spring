package com.michael.coupon.web.resources;

import com.michael.coupon.web.security.AppUser;
import com.michael.coupon.web.security.UserInfo;
import com.michael.coupons.beans.Company;
import com.michael.coupons.facades.AdminFacade;
import com.michael.coupons.facades.ICouponClientFacade;
import java.net.URI;
import java.util.Objects;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.michael.coupon.web.utils.exceptions.JSONErrorMessage;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.Exceptions;

import javax.inject.Singleton;

/**
 * CompanyResource is the resource for Company related operations mapped to
 * /companies
 *
 * @author michael
 */
@PermitAll
@Singleton
@Path("/companies")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CompanyResource {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    // ======================PARAMETERS=======================
    // Injecting the security pricipal that holds clientFacade.
    @AppUser  
    UserInfo userInfo;

    // ======================METHODS==========================
    /**
     * 
     * @param comp_id
     * @return
     */
    @RolesAllowed("ADMIN")
    @GET
    @Path("/{comp_id}")
    public Response getCompany(@PathParam("comp_id") Long comp_id) {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).getCompany(comp_id)).build();

    }

    @RolesAllowed("ADMIN")
    @GET
    public Response getAllCompanies() {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).getAllCompanies()).build();
    }

    /**
     *
     * @param company
     * @param uriInfo
     * @return
     */
    @RolesAllowed("ADMIN")
    @POST
    public Response createCompany(Company company, @Context UriInfo uriInfo) {

        Company newCompany = ((AdminFacade) userInfo.getClientFacade()).createCompany(company);

        String newId = String.valueOf(newCompany.getId());

        //create a location header uri pointing to newly created company    
        URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
        return Response.created(uri)
                .entity(newCompany)
                .build();
    }

    /**
     *
     * @param comp_id
     * @param company
     * @return
     */
    @RolesAllowed({"ADMIN", "COMPANY"})
    @PUT
    @Path("/{comp_id}")
    public Response updateCompany(@PathParam("comp_id") Long comp_id, Company company) {

        if (!company.getId().equals(comp_id)) {

            throw new ApplicationException(304, "Object ID [" + company.getId() + "] doesn't coresponds to URL ID provided [" + String.valueOf(comp_id) + "]");

        }
        Company updatedCompany;

        ICouponClientFacade clientFacade = userInfo.getClientFacade();

        if (clientFacade instanceof AdminFacade) {
            updatedCompany = ((AdminFacade) clientFacade).updateCompany(company);
        } else {
            return Response.status(Status.UNAUTHORIZED)
                    .entity(new JSONErrorMessage(401, Exceptions.INVALID_LOGIN_TYPE, "Please login with Admin privileges.", "Unauthorized access", "updateCompany"))
                    .build();
        }

        return Response.ok(updatedCompany)
                //.entity(newCompany)
                .build();
    }

    /**
     *
     * @param comp_id
     * @param company
     * @return
     */
    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/{comp_id}")
    public Response deleteCompany(@PathParam("comp_id") Long comp_id, Company company) {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).removeCompany(comp_id)).build();

    }

    /**
     *
     * @return
     */
    @Path("/{comp_id}/coupons")
    public CouponResource getCouponResource() {
        return new CouponResource();
    }

}
