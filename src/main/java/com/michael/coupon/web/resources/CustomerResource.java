package com.michael.coupon.web.resources;

import com.michael.coupon.web.security.AppUser;
import com.michael.coupon.web.security.UserInfo;
import com.michael.coupons.beans.Customer;
import com.michael.coupons.facades.AdminFacade;
import com.michael.coupons.facades.ICouponClientFacade;
import java.net.URI;
import java.util.Objects;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;
import com.michael.coupon.web.utils.exceptions.JSONErrorMessage;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.utils.Exceptions;

import javax.inject.Singleton;
import javax.ws.rs.OPTIONS;

/**
 * CustomerResource is the resource for Customer related operations mapped to
 * /customers
 *
 * @author michael
 */
@PermitAll
@Singleton
@Path("/customers")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class CustomerResource {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    // ======================PARAMETERS=======================
    // Injecting the security pricipal that holds clientFacade.
    @AppUser
    UserInfo userInfo;

    // ======================METHODS==========================
    
//    @OPTIONS
//    public Response getOptions() {
//    
//        return Response.ok().header("Access-Control-Allow-Credentials", "*").build();
//    }
    /**
     *
     * @param cust_id
     * @return
     */
    @RolesAllowed("ADMIN")
    @GET
    @Path("/{cust_id}")
    public Response getCustomer(@PathParam("cust_id") Long cust_id) {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).getCustomer(cust_id)).build();

    }

    /**
     *
     * @return
     */
    @RolesAllowed("ADMIN")
    @GET
    public Response getAllCustomers() {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).getAllCustomers()).build();
    }

    /**
     *
     * @param customer
     * @param uriInfo
     * @return
     */
    @RolesAllowed("ADMIN")
    @POST
    public Response createCustomer(Customer customer, @Context UriInfo uriInfo) {

        Customer newCustomer = ((AdminFacade) userInfo.getClientFacade()).createCustomer(customer);

        String newId = String.valueOf(newCustomer.getId());

        //create a location header uri pointing to newly created customer    
        URI uri = uriInfo.getAbsolutePathBuilder().path(newId).build();
        return Response.created(uri)
                .entity(newCustomer)
                .build();
    }

    /**
     *
     * @param cust_id
     * @param customer
     * @return
     */
    @RolesAllowed({"ADMIN", "CUSTOMER"})
    @PUT
    @Path("/{cust_id}")
    public Response updateCustomer(@PathParam("cust_id") Long cust_id, Customer customer) {

        if (!Objects.equals(cust_id, customer.getId())) {

            throw new ApplicationException(304, "Object ID [" + customer.getId() + "] doesn't coresponds to URL ID provided [" + String.valueOf(cust_id) + "]");

        }
        Customer updatedCustomer;

        ICouponClientFacade clientFacade = userInfo.getClientFacade();

        if (clientFacade instanceof AdminFacade) {
            updatedCustomer = ((AdminFacade) clientFacade).updateCustomer(customer);
        } else {
            return Response.status(Status.UNAUTHORIZED)
                    .entity(new JSONErrorMessage(401, Exceptions.INVALID_LOGIN_TYPE, "Please login with Admin privileges.", "Unauthorized access", "updateCustomer"))
                    .build();
        }

        return Response.ok(updatedCustomer)
                //.entity(newCustomer)
                .build();
    }

    /**
     *
     * @param cust_id
     * @param customer
     * @return
     */
    @RolesAllowed("ADMIN")
    @DELETE
    @Path("/{cust_id}")
    public Response deleteCustomer(@PathParam("cust_id") Long cust_id, Customer customer) {

        return Response.accepted(((AdminFacade) userInfo.getClientFacade()).removeCustomer(cust_id)).build();

    }

    /**
     *
     * @return
     */
    @Path("/{cust_id}/coupons")
    public CouponResource getCouponResource() {
        return new CouponResource();
    }

    
    
}
