package com.michael.coupon.web.resources;

import com.michael.coupon.web.security.AppUser;
import com.michael.coupon.web.security.UserInfo;
import com.michael.coupons.beans.Coupon;
import com.michael.coupons.entities.Category;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.facades.AdminFacade;
import com.michael.coupons.facades.CompanyFacade;
import com.michael.coupons.facades.CustomerFacade;
import com.michael.coupons.facades.ICouponClientFacade;
import java.util.Collection;
import javax.annotation.Resource;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.Queue;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@PermitAll
@Singleton
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
@Path("/coupons")
public class CouponResource {

    // ======================CONSTANTS========================
    private static final long serialVersionUID = 1L;

    // ======================PARAMETERS=======================
//    @Resource(lookup = "jms/MyQueue")
//    private Queue queue;
//    @Inject
//    private JMSContext jmsContext;

    //@AppUser UserInfo userInfo;
    /**
     *
     * @param userInfo
     * @param coupon_id
     * @return
     */
    @GET
    @Path("/{coupon_id}")
    public Response getCoupon(@AppUser UserInfo userInfo, @PathParam("coupon_id") Long coupon_id) {

        Coupon coupon = null;

        switch (userInfo.getRole()) {
            case ADMIN:
                coupon = ((AdminFacade) userInfo.getClientFacade()).getCoupon(coupon_id);
                break;
            case COMPANY:
                coupon = ((CompanyFacade) userInfo.getClientFacade()).getCoupon(coupon_id);
                break;
            case CUSTOMER:
                coupon = ((CustomerFacade) userInfo.getClientFacade()).getCoupon(coupon_id);
                break;
        }

        if (coupon == null) {
            throw new ApplicationException(404, 520, "Error in getting coupon object, clientFacade is missing");
        }

        return Response
                .ok(coupon)
                .build();
    }

    /**
     *
     * @param userInfo
     * @param coupon_id
     * @return
     */
    @RolesAllowed("CUSTOMER")
    @GET
    @Path("/{coupon_id}/purchase")
    public Response purchaseCoupon(@AppUser UserInfo userInfo, @PathParam("coupon_id") Long coupon_id) {

//        String msg = "My JMS 2.0 Message";
//        jmsContext.createProducer().send(queue, msg);

        return Response
                .ok(((CustomerFacade) userInfo.getClientFacade()).purchaseCoupon(userInfo.getMyId(), coupon_id))
                .build();

    }

    /**
     *
     * @param userInfo
     * @param coupon
     * @return
     */
    @RolesAllowed("COMPANY")
    @POST
    public Response createCoupon(@AppUser UserInfo userInfo, Coupon coupon) {

        return Response
                .ok(((CompanyFacade) userInfo.getClientFacade()).createCoupon(userInfo.getMyId(), coupon))
                .build();
    }

    /**
     * 
     * @param userInfo
     * @param comp_id
     * @param cust_id
     * @param price
     * @param type
     * @param date
     * @return
     */
    @GET
    public Response getAllCoupons(
            @AppUser UserInfo userInfo,
            @PathParam("comp_id") Long comp_id,
            @PathParam("cust_id") Long cust_id,
            @QueryParam("price") Double price,
            @QueryParam("type") Category type,
            @QueryParam("date") String date
    ) {

        Collection<Coupon> coupons = null;

        ICouponClientFacade clientFacade = userInfo.getClientFacade();

        switch (userInfo.getRole()) {

            case ADMIN:
                if (comp_id != null) {
                    //Get all company's coupons 
                    coupons = ((AdminFacade) clientFacade).getCompany(comp_id).getCoupons();
                } else if (cust_id != null) {
                    //Get all customer's coupons
                    coupons = ((AdminFacade) clientFacade).getCustomer(cust_id).getCoupons();
                } else {
                    //Get all existing coupons
                    coupons = ((AdminFacade) clientFacade).getAllCoupons();
                }
                break;

            case COMPANY:
               
                    if (price != null) {
                        //Get company coupons by price filter
                        coupons = ((CompanyFacade) clientFacade).getCouponByPrice(userInfo.getMyId(), price);
                    } else if (type != null) {
                         //Get company coupons by type filter
                        coupons = ((CompanyFacade) clientFacade).getCouponByType(userInfo.getMyId(), type);
                    } else if (date != null) {
                         //Get company coupons by date filter
                        coupons = ((CompanyFacade) clientFacade).getCouponByDate(userInfo.getMyId(), date);
                    } else {
                         //Get all company's coupons
                     coupons = ((CompanyFacade) clientFacade).getAllCoupons(userInfo.getMyId());
                    //throw new ApplicationException(403, 403, "You can only see your coupons");
                }
                break;

            case CUSTOMER:
                if (userInfo.getMyId().equals(cust_id)) {
                    if (price != null) {
                        coupons = ((CustomerFacade) clientFacade).getAllPurchasedCouponsByPrice(cust_id, price);
                    } else if (type != null) {
                        coupons = ((CustomerFacade) clientFacade).getAllPurchasedCouponsByType(cust_id, type);
                    } else {
                        coupons = ((CustomerFacade) clientFacade).getAllPurchasedCoupons(cust_id);
                    }
                } else if (cust_id != null) {
                    throw new ApplicationException(403, 403, "You can only see your coupons");
                } else {
                    coupons = ((CustomerFacade) clientFacade).getAllCoupons();
                }
                break;

        }

        if (coupons == null || coupons.isEmpty()) {
            throw new ApplicationException(404, 520, "No coupons exists");
        }
        return Response.ok(coupons).build();
    }

    /**
     *
     * @param userInfo
     * @param coupon_id
     * @param coupon
     * @return
     */
    @RolesAllowed(value = "COMPANY")
    @PUT
    @Path(value = "/{coupon_id}")
    public Response updateCoupon(@AppUser UserInfo userInfo, @PathParam(value = "coupon_id") Long coupon_id, Coupon coupon) {
        if (!coupon.getId().equals(coupon_id)) {

            throw new ApplicationException(304, "Object ID [" + coupon.getId() + "] doesn't coresponds to URL ID provided [" + String.valueOf(coupon_id) + "]");

        }

        return Response
                .ok(((CompanyFacade) userInfo.getClientFacade()).updateCoupon(userInfo.getMyId(), coupon))
                .build();
    }

    /**
     *
     * @param userInfo
     * @param coupon_id
     * @return
     */
    @RolesAllowed(value = "COMPANY")
    @DELETE
    @Path(value = "/{coupon_id}")
    public Response deleteCoupon(@AppUser final UserInfo userInfo, @PathParam("coupon_id") Long coupon_id) {
        return Response
                .ok(((CompanyFacade) userInfo.getClientFacade()).removeCoupon(userInfo.getMyId(), coupon_id))
                .build();
    }

}
