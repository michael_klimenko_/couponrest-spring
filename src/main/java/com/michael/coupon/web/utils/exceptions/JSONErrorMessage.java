
package com.michael.coupon.web.utils.exceptions;

import com.michael.coupons.utils.Exceptions;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Custom message object used in REST JSON response when exception is thrown
 * @author michael
 */
@XmlRootElement
public class JSONErrorMessage {
    
    
    private int errorCode;
    private Exceptions type;
    private String errorMessage;
    private String errorCause;
    private String methodSource;
    

    

    public JSONErrorMessage() {
    }

    public JSONErrorMessage(int errorCode,Exceptions type, String errorMessage, String errorCause, String methodSource) {
        this.errorCode = errorCode;
        this.type = type;
        this.errorMessage = errorMessage;
        this.errorCause = errorCause;
        this.methodSource = methodSource;
    }

    
    public int getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public Exceptions getType() {
        return type;
    }

    public void setType(Exceptions type) {
        this.type = type;
    }

    
    
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCause() {
        return errorCause;
    }

    public void setErrorCause(String errorCause) {
        this.errorCause = errorCause;
    }

    public String getMethodSource() {
        return methodSource;
    }

    public void setMethodSource(String methodSource) {
        this.methodSource = methodSource;
    }

    
    

}
