/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.michael.coupon.web.utils.exceptions;

/**
 * Login response object (need for converting response to JSON)
 * @author michael
 */
public class LoginResponse {
    
    private String result;
    private Boolean allowed;

    public LoginResponse() {
        
    }

    public LoginResponse(String result, Boolean allowed) {
        this.result = result;
        this.allowed = allowed;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public Boolean getAllowed() {
        return allowed;
    }

    public void setAllowed(Boolean allowed) {
        this.allowed = allowed;
    }
    
}
