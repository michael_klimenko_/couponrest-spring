package com.michael.coupon.web.utils.exceptions;


import com.michael.coupons.exceptions.ApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;


/**
 * ApplicationExceptionMapper catches all exceptions thrown by web service or business logic.
 * Helps to eliminate HTML response in REST Resources.
 * @author michael
 */
@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException>{
   
    @Override
    public Response toResponse(ApplicationException exception) {
    
   
        JSONErrorMessage errorMsg = new JSONErrorMessage(
                exception.getErrorCode(),
                exception.getType(),
                exception.getMessage(),
                exception.getClass().getName(),
                exception.getStackTrace()[0].getFileName() +":"+ exception.getStackTrace()[0].getLineNumber() 
                        + "(" +  exception.getStackTrace()[0].getMethodName() +")"
        );
        return Response
                .status(exception.getHttpStatus())
                .header("Content-Type", "application/json")
                .entity(errorMsg)
                .build();
    }
    
}
