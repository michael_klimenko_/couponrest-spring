
package com.michael.coupon.web.utils;

import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author michael
 */
public enum ErrorType {
    
    INVALID_USER_CRIDENTIALS(HttpServletResponse.SC_BAD_REQUEST,700),
    PASSWORD_TOO_SHORT(HttpServletResponse.SC_BAD_REQUEST,704),
    PASSWORD_TOO_LOW(HttpServletResponse.SC_BAD_REQUEST,705);
        
    private final int httpErrorCode;
    private final int internalErrorCode;

    private ErrorType(int httpErrorCode, int internalErrorCode) {
        this.httpErrorCode = httpErrorCode;
        this.internalErrorCode = internalErrorCode;
    }

    public int getHttpErrorCode() {
        return httpErrorCode;
    }

    public int getInternalErrorCode() {
        return internalErrorCode;
    }

    
    

}
