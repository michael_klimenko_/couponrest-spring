package com.michael.coupon.web.filters;

import static com.michael.coupons.utils.ApplicationUtilities.log;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;

/**
 * Filter that allows CORS request for specific domains
 * @author michael
 */

@Provider
public class CORSResponseFilter implements ContainerResponseFilter {

    //private static final List<String> ALLOWED_DOMAINS = new ArrayList<>();

    public CORSResponseFilter() {
        super();
        //ALLOWED_DOMAINS.add("localhost:8080");
        //ALLOWED_DOMAINS.add("localhost:8080");
    }
    
    
    
    
    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
            throws IOException {

        log("Request method : " + requestContext.getMethod());
        log("Request headers before : " + requestContext.getHeaders());
        log("Response headers before : " + responseContext.getHeaders());
        //Allow CORS requests only for OPTIONS
       // if (requestContext.getMethod().equals("OPTIONS")) {

            
            MultivaluedMap<String, Object> headers = responseContext.getHeaders();

            headers.add("Access-Control-Allow-Origin", "*");
            //headers.add("Access-Control-Allow-Origin", "http://podcastpedia.org"); //allows CORS requests only coming from podcastpedia.org		
            //headers.add("Access-Control-Allow-Origin", "http://localhost:8081"); //allows CORS requests only coming from localhost:8081		
            headers.add("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT");
            headers.add("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Authorization");

            log("Filter: added to response headers : Access-Control-Allow- ....");
       // }
        log("Request headers after : " + requestContext.getHeaders());
        log("Response headers after : " + responseContext.getHeaders());
    }

}
