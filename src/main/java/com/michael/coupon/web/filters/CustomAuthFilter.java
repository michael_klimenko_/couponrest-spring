package com.michael.coupon.web.filters;

import com.michael.coupon.web.security.UserInfo;
import com.michael.coupon.web.security.UserSecurityContext;
import com.michael.coupons.utils.ApplicationUtilities;

import com.michael.coupons.CouponSystem;
import com.michael.coupons.exceptions.ApplicationException;
import com.michael.coupons.facades.ICouponClientFacade;
import static com.michael.coupons.utils.ApplicationUtilities.log;

import com.michael.coupons.utils.ClientType;
import com.michael.coupons.utils.Exceptions;
import javax.ws.rs.container.PreMatching;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.regex.Pattern;

import javax.annotation.Priority;
import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import com.nimbusds.jose.JWSVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWT;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTParser;
import com.nimbusds.jwt.SignedJWT;
import javax.ws.rs.core.HttpHeaders;

@Provider
@PreMatching
// default priority is 'USER' which is too late in the filter processing chain
@Priority(Priorities.AUTHORIZATION)
public class CustomAuthFilter implements ContainerRequestFilter {


    //=====================PARAMETERS===================================
    UserInfo userInfo = null;

    //JWToken in HTTP Header 
    private final Pattern tokenPattern = Pattern.compile("^Bearer$", Pattern.CASE_INSENSITIVE);
    private JWSVerifier jwsVerifier;

    /**
     * Instantiates a new JWT verifier with signing secret.
     * 
     * @throws Exception in case of error setting up JWS Verifier
     */
    public CustomAuthFilter() throws Exception {
        
        log("Initializing Custom JWT Authorization Filter...");
        
        String keystore = ApplicationUtilities.CONFIG.getString("api.security.keystore.file");
        String password = ApplicationUtilities.CONFIG.getString("api.security.keystore.password");
        String alias = ApplicationUtilities.CONFIG.getString("api.security.key.alias");

        log("Trying to load public key : " + keystore + " " + password + " " + alias);
        PublicKey publicKey = loadPublicKey(keystore, password, alias);

        log("public key loaded : " + publicKey.toString() + " " + publicKey.getAlgorithm() + " " + publicKey.getFormat());
        if (publicKey != null) {
            jwsVerifier = new RSASSAVerifier((RSAPublicKey) publicKey);
        } else {
            throw new RuntimeException("Configuration error: unable to load JWT signing public key from keystore: " + keystore);
        }
    }

    /**
     * Gets public key from a JKS keystore.
     *
     * @param keystoreFile The keystore file pathname
     * @param password The keystore password
     * @param alias The key alias name
     * @return {@link RSAPublicKey} for the alias if found, null otherwise
     */
    private PublicKey loadPublicKey(String keystoreFile, String password, String alias) {
        PublicKey publicKey = null;
        log("Loading public key: {} from keystore: " + alias + " - " + keystoreFile);
        try {
            KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());

            File file = new File(keystoreFile);
            
            log("Loading public key: {} from file: " + file.getAbsolutePath() + " : " + file.getPath());
            InputStream is = null;
            if (file.exists()) {
                is = new BufferedInputStream(new FileInputStream(file));
            } else {
                is = getClass().getResourceAsStream(keystoreFile);
            }

            if (is != null) {
                keystore.load(is, password.toCharArray());
                Certificate cert = keystore.getCertificate(alias);
                if (cert != null) {
                    publicKey = cert.getPublicKey();
                } else {
                    log("Invalid key alias provided, key not found");
                }
            } else {
                log("Unable to load keystore file: {}" + keystoreFile);
            }
        } catch (Exception e) {
            log(e.getMessage() + e);
        }
        return publicKey;
    }

    /**
     * Custom filter method that verify JW token exists and is valid.
     * 
     * (non-Javadoc)
     * @throws java.io.IOException
     * @see javax.ws.rs.container.ContainerRequestFilter#filter(javax.ws.rs.container.ContainerRequestContext)
     */
    @Override
    public void filter(final ContainerRequestContext requestContext) throws IOException {
        
        
        if (!requestContext.getUriInfo().getPath().equals("login") && !requestContext.getMethod().equals("OPTIONS")) {
            String authorizationHeader = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
            log("Filter: authorizationHeader: " + authorizationHeader);
            if (authorizationHeader != null) {
                String token = parseBearerToken(authorizationHeader);
                log("Filter: token: " + token);
                if (token != null) {
                    JWTClaimsSet claims = validateToken(token);
                    log("Filter: claims: " + claims.toString());
                    //JWTPrincipal principal = buildPrincipal(claims);
                    UserInfo principal = buildUserPrincipal(claims);
                    log("Filter: principal: " + principal.toString());
                    if (principal != null) {
                        // Build and inject SecurityContext for @RoleAllowed, isUserInRole(), getUserPrincipal() to work
                        UserSecurityContext ctx = new UserSecurityContext(principal,requestContext.getSecurityContext().isSecure());
                        requestContext.setSecurityContext(ctx);
                    } else {
                        throw new NotAuthorizedException(
                                "Unauthorized: Unable to extract claims from JWT",
                                Response.status(Status.UNAUTHORIZED));
                    }
                } else {
                    throw new NotAuthorizedException(
                            "Unauthorized: Unable to parse Bearer token",
                            Response.status(Status.UNAUTHORIZED));
                }
            } else {
                throw new NotAuthorizedException(
                        "Unauthorized: No Authorization header was found",
                        Response.status(Status.UNAUTHORIZED));
            }
        }
    }
    
     /**
     * Creates a new instance of {@link JWTPrincipal} from JSON Web Token (JWT)
     * claims.
     *
     * <pre>
     * JWT: {
     *  ...
     * "use": "john.doe@idp.com",
     * "name": "John Doe",
     * "family_name": "Doe",
     * "given_name": "John"
     * }
     * </pre>
     *
     * @param claims The JWT claims set
     * @return A new instance of {@link JWTPrincipal} from JSON Web Token (JWT)
     * claims
     */
    private UserInfo buildUserPrincipal(final JWTClaimsSet claims) {
        UserInfo principal = null;

        try {
            if (claims != null) {
                String name = claims.getSubject();
                Long myId = (Long) claims.getClaim("myId");
                ClientType role = ClientType.valueOf((String)claims.getClaim("role"));
                ICouponClientFacade clientFacade = CouponSystem.getInstance().login(name,"",role,false);
                
                // TODO: Extract custom attributes, e.g. roles, organization affiliation etc. and put into principal.
                principal = new UserInfo(myId, name, role, clientFacade);
             
            }
        } catch (Exception e) {
            log(e.getMessage() + " " + e);
        }
        return principal;
    }

    /**
     * Validate the JSON Web Token for signature, expiration and not before
     * time.
     *
     * @param token The JSON Web Token
     * @return {@link JWTClaimsSet} in case of success, null otherwise
     */
    private JWTClaimsSet validateToken(final String token) {
        JWTClaimsSet claims = null;

        try {
            JWT jwt = JWTParser.parse(token);
            if (jwt instanceof SignedJWT) {
                SignedJWT signedJWT = (SignedJWT) jwt;
                if (signedJWT.verify(jwsVerifier)) {
                    claims = signedJWT.getJWTClaimsSet();
                    log("JWT claims: {} " + claims.getClaims());
                    Date expirationTime = claims.getExpirationTime();
                    Date now = new Date();
                    Date notBeforeTime = claims.getNotBeforeTime();
                    if (notBeforeTime.compareTo(now) > 0) {
                        throw new ApplicationException(Exceptions.UNAUTHORIZED, 401, Status.UNAUTHORIZED.getStatusCode(), "Unauthorized: too early, token not valid yet");
                    }
                    if (expirationTime.compareTo(now) <= 0) {
                        throw new ApplicationException(Exceptions.UNAUTHORIZED, 401, Status.UNAUTHORIZED.getStatusCode(), "Unauthorized: too late, token expired");
                    }
                } else {
                    throw new ApplicationException(Exceptions.UNAUTHORIZED, 401, Status.UNAUTHORIZED.getStatusCode(), "Unauthorized: Unable to verify Bearer token");
                }
            } else {
                throw new ApplicationException(Exceptions.UNAUTHORIZED, 401, Status.UNAUTHORIZED.getStatusCode(), "Unauthorized: Unexpected JWT type");
            }
            
        } catch (Exception e) {
            throw new ApplicationException(Exceptions.UNAUTHORIZED, e, 401, Status.UNAUTHORIZED.getStatusCode());

        }
        return claims;
    }

    /**
     * Extract Bearer token value from string "Bearer [value]".
     *
     * @param bearerToken The Bearer token string of the form "Bearer [value]"
     * @return The value part of the token if scheme (prefix) matches with
     * Bearer, null otherwise
     */
    private String parseBearerToken(final String bearerToken) {
        String tokenValue = null;
        if (bearerToken != null) {
            String[] parts = bearerToken.split(" ");
            if (parts.length == 2) {
                String scheme = parts[0];
                String credentials = parts[1];
                if (tokenPattern.matcher(scheme).matches()) {
                    tokenValue = credentials;
                }
            }
        }
        return tokenValue;
    }

}
