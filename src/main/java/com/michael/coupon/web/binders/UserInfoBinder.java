package com.michael.coupon.web.binders;

import com.michael.coupon.web.security.AppUser;
import com.michael.coupon.web.providers.UserInfoFactoryProvider;
import javax.inject.Singleton;
import org.glassfish.hk2.api.InjectionResolver;
import org.glassfish.hk2.api.TypeLiteral;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.spi.internal.ValueFactoryProvider;

/**
 * Binder to inject UserInfo principal
 * @author michael
 */
public class UserInfoBinder extends AbstractBinder {

        @Override
        protected void configure() {
            bind(UserInfoFactoryProvider.class).to(ValueFactoryProvider.class).in(Singleton.class);
            bind(UserInfoFactoryProvider.InjectionResolver.class)
                    .to(new TypeLiteral<InjectionResolver<AppUser>>() {
                    }).in(Singleton.class);
        }
    }
