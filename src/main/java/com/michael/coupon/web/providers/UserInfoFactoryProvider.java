
package com.michael.coupon.web.providers;

import com.michael.coupon.web.security.AppUser;
import com.michael.coupon.web.security.UserInfo;
import java.security.Principal;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.jersey.server.internal.inject.AbstractContainerRequestValueFactory;
import org.glassfish.jersey.server.internal.inject.AbstractValueFactoryProvider;
import org.glassfish.jersey.server.internal.inject.MultivaluedParameterExtractorProvider;
import org.glassfish.jersey.server.internal.inject.ParamInjectionResolver;
import org.glassfish.jersey.server.model.Parameter;
import static com.michael.coupons.utils.ApplicationUtilities.log;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author michael
 */
@Singleton
public class UserInfoFactoryProvider extends AbstractValueFactoryProvider {

    @Inject
    public UserInfoFactoryProvider(final MultivaluedParameterExtractorProvider mpep, ServiceLocator locator ) {
        super(mpep, locator, Parameter.Source.UNKNOWN);
    }

    @Singleton
    public static class InjectionResolver extends ParamInjectionResolver<AppUser> {
        public InjectionResolver() {
            super(UserInfoFactoryProvider.class);
        }
    }

    private static final class UserInfoFactory extends AbstractContainerRequestValueFactory<UserInfo> {

        
        @Override
        public UserInfo provide() {
            final Principal principal = getContainerRequest().getSecurityContext().getUserPrincipal();
           //log("Providing user info: {}" + principal.toString());
            return (UserInfo) principal;
        }
    }

    @Override
    protected AbstractContainerRequestValueFactory<?> createValueFactory(Parameter parameter) {
        return new UserInfoFactory();
    }


}